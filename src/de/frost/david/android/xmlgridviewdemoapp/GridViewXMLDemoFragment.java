package de.frost.david.android.xmlgridviewdemoapp;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import de.frost.david.android.xmlgridviewdemoapp.model.Food;
import de.frost.david.android.xmlgridviewdemoapp.model.Menu;

public class GridViewXMLDemoFragment extends Fragment {
	private GridView mGridView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_grid_view_xmldemo, container, false);

		mGridView = (GridView)v.findViewById(R.id.gridview);

		new XmlAsyncLoader().execute("http://frost.serverbros.co.uk/androidTemp/menu.xml"); // http://www.androidpeople.com/wp-content/uploads/2010/06/example.xml

		return v;
	}

	public Menu xmlToMenuWithFood(Node node) {
		Menu menu = new Menu();
		
		NodeList nl = node.getChildNodes();
		
		
		String res_name = "" + nl.item(1).getFirstChild().getNodeValue();
		String update_version = "" + nl.item(3).getChildNodes().item(1).getFirstChild().getNodeValue();
		String update_date = "" + nl.item(3).getChildNodes().item(3).getFirstChild().getNodeValue();
		
		menu.setRestaurant(res_name);
		menu.setDate(update_date);
		menu.setVersion(update_version);
		
		NodeList menu_nl = nl.item(5).getChildNodes();
		
		for (int i = 0; i < menu_nl.getLength(); i++) {
			Node n = menu_nl.item(i);

			if (n.getNodeType() == Node.ELEMENT_NODE) {
				if (n.getNodeName().equals("food")) {
					menu.insertFood(extractFood(n.getChildNodes()));
				} 
			}
		}
		
		return menu;
	}

	public Food extractFood(NodeList nl) {
		Food f = new Food();

		for (int i = 0; i < nl.getLength(); i++) {
			Node n = nl.item(i);

			if (n.getNodeType() == Node.ELEMENT_NODE) {
				Node child = n.getFirstChild();
				
				if (n.getNodeName().equals("name")) {
					f.setName(child.getNodeValue());
				} else if (n.getNodeName().equals("price")) {
					f.setPrice(child.getNodeValue());
				} else if (n.getNodeName().equals("description")) {
					f.setDescription(child.getNodeValue());
				}
			}
		}
		
		return f;
	}

	private class XmlAsyncLoader extends AsyncTask<String, Void, Menu> {

		@Override
		protected Menu doInBackground(String... urls) {
			Menu m = null;
			try {
				URL url = new URL(urls[0]);

				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();
				Document doc = db.parse(new InputSource(url.openStream()));
				doc.getDocumentElement().normalize();

				NodeList nodeList = doc.getChildNodes();

				m = xmlToMenuWithFood(nodeList.item(0));

			} catch (MalformedURLException e) {
				Log.e("xml", "MalformedURLException" + e.getMessage());
			} catch (ParserConfigurationException e) {
				Log.e("xml", "ParserConfigurationException" + e.getMessage());
			} catch (SAXException e) {
				Log.e("xml", "SAXException" + e.getMessage());
			} catch (IOException e) {
				Log.e("xml", "IOException" + e.getMessage());
			}

			return m;
		}

		@Override
		protected void onPostExecute(Menu m) {
			mGridView.setAdapter(new GridAdapter(getActivity(), m));
		}

		@Override
		protected void onPreExecute() {
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}
	}

}
