package de.frost.david.android.xmlgridviewdemoapp;

import android.support.v4.app.Fragment;

public class GridViewXMLDemoActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new GridViewXMLDemoFragment();
	}

}
