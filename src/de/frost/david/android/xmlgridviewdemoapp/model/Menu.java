package de.frost.david.android.xmlgridviewdemoapp.model;

import java.util.ArrayList;

public class Menu {
	private String version;
	private String date;
	private String restaurant;
	private ArrayList<Food> foodlist;
	
	
	public Menu() {
		foodlist = new ArrayList<Food>();
	}
	
	public void insertFood(Food f) {
		foodlist.add(f);
	}
	
	public Food getFood(int i) {
		return foodlist.get(i);
	}
	
	public int getFoodListSize() {
		return foodlist.size();
	}

	public String getVersion() {
		return version;
	}

	public String getDate() {
		return date;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(String restaurant) {
		this.restaurant = restaurant;
	}
	
	public String toString() {
		String fff = "";
		
		for (Food f: foodlist) {
			fff += f + "\n";
		}
		
		return "Restaurante: " + this.restaurant +
				"\nVersion: " + this.version + 
				"\nDate: " + this.date + 
				"\n#Foods: " + this.foodlist.size() + 
				"\nFoods: " + fff;
	}
	

}
