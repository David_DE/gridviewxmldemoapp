package de.frost.david.android.xmlgridviewdemoapp.model;

public class Food {
	private String name;
	private String description;
	private String price;
	
	public Food() {
		
	}
	
	public Food(String name, String description, String price) {
		this.name = name;
		this.description = description;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getPrice() {
		return price;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	public String toString() {
		return this.name +
				"\n" + this.price;
	}

}
