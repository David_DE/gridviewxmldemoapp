package de.frost.david.android.xmlgridviewdemoapp;

import de.frost.david.android.xmlgridviewdemoapp.model.Menu;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

public class GridAdapter extends BaseAdapter{
	private Context mContext;
	private Menu mMenu;
	
	public GridAdapter(Context c, Menu m) {
		mContext = c;
		mMenu = m;
	}

	@Override
	public int getCount() {
		return mMenu.getFoodListSize();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView textView;
		
		if (convertView == null) {
			textView = new TextView(mContext);
			textView.setLayoutParams(new GridView.LayoutParams(150, 150));
			textView.setPadding(8, 8, 8, 8);
		} else {
			textView = (TextView)convertView;
		}
		
		textView.setText(""+mMenu.getFood(position));
		
		return textView;
	}

}
